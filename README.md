# Movie service
This app uses api that provides information about movies and series. [Deployed Static Site](https://movee-lnsk.onrender.com/)

### Technologies:
* React.js
* REST API
* Axios
* Scss

## Start the program:
* Execute the `npm install` and `npm start` command in root directory.